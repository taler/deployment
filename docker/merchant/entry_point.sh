#!/bin/bash
 

(su -c "createuser --host=dbcontainer root" - postgres | exit 0)
(su -c "createdb --host=dbcontainer talertest" - postgres | exit 0)
taler-config -s merchantdb-postgres -o config \
  -V "dbname=talertest host=dbcontainer"
taler-config -s merchant -o serve -V tcp
taler-config -s merchant -o port -V 9966
taler-config -s merchant-exchange-test -o master_key -V $(cat /exchange_pub.txt|tr -d '\n')
taler-config -s merchant-exchange-test -o url -V $(cat /exchange_url.txt | tr -d '\n')
taler-merchant-dbinit -r
taler-merchant-httpd
