#!/bin/bash
 

if ! test -a $HOME/shared-data/exchange/offline-keys/master.priv; then
  echo "Regenerating all keys and db entries"
  (su -c "createuser --host=dbcontainer root" - postgres | exit 0)
  (su -c "createdb --host=dbcontainer talertest" - postgres | exit 0)
  mkdir -p $HOME/shared-data/exchange/offline-keys/
  gnunet-ecc -g1 $HOME/shared-data/exchange/offline-keys/master.priv
  taler-config -s exchangedb-postgres -o db_conn_str \
    -V "dbname=talertest host=dbcontainer"
  taler-config -s exchange -o serve -V tcp
  taler-config -s exchange -o port -V 8081
  taler-config -s exchange-admin -o serve -V tcp
  taler-config -s exchange-admin -o port -V 18080
  taler-config -s exchange -o master_public_key \
    -V $(gnunet-ecc -p $HOME/shared-data/exchange/offline-keys/master.priv)
  $HOME/deployment/bin/taler-deployment-config-sign
  $HOME/deployment/bin/taler-deployment-keyup
  taler-exchange-dbinit -r
fi

taler-exchange-httpd
