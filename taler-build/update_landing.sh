#!/bin/bash

set -eu

cd $HOME/landing
git clean -fxd

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

git submodule update --force --init
AUTOMAKE="automake --foreign" autoreconf -fiv
./configure
cd demo/
make
