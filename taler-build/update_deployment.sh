#!/bin/bash

# This update script is needed since shell scripts called
# by buildbot do NOT use the fresh deployment repo which is
# internal to buildbot, so we need to update the $HOME/deployment
# local check before starting the whole compilation

set -eu

cd $HOME/deployment

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"
