#!/bin/bash

cd $HOME/gnurl
git clean -fxd

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

./buildconf
./configure --enable-ipv6 --with-gnutls --without-libssh2 --without-libmetalink --without-winidn --without-librtmp --without-nghttp2 --without-nss --without-cyassl --without-polarssl --without-ssl --without-winssl --without-darwinssl --disable-sspi --disable-ntlm-wb --disable-ldap --disable-rtsp --disable-dict --disable-telnet --disable-tftp --disable-pop3 --disable-imap --disable-smtp --disable-gopher --disable-file --disable-ftp --disable-smb --prefix=$HOME/local
make
make install
