#!/bin/bash

set -eu

cd $HOME/exchange
git clean -fdx

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

./bootstrap
if test ${1:-notgiven} = "--coverage"; then
  ./configure CFLAGS='-ggdb -O0' \
          --with-libgnurl=$HOME/local \
          --with-microhttpd=$HOME/local \
          --with-twister=$HOME/local \
          --prefix=$HOME/local --with-gnunet=$HOME/local \
          --enable-logging=verbose \
          --enable-coverage
else
  ./configure CFLAGS='-ggdb -O0' \
          --with-libgnurl=$HOME/local \
          --with-microhttpd=$HOME/local \
          --with-twister=$HOME/local \
          --prefix=$HOME/local --with-gnunet=$HOME/local \
          --enable-logging=verbose
fi

make install -j4

if test ${1:-notgiven} = "--coverage"; then
TOP=$(pwd)
  mkdir -p doc/coverage/
  lcov -d $TOP -z
  TALER_AUDITORDB_POSTGRES_CONFIG=$TALER_CHECKDB \
  TALER_EXCHANGEDB_POSTGRES_CONFIG=$TALER_CHECKDB \
  TALER_BANK_ALTDB=$TALER_CHECKDB make check || exit 1
  lcov -d $TOP -c --no-external -o doc/coverage/coverage.info
  lcov -r doc/coverage/coverage.info **/test_* -o doc/coverage/rcoverage.info
  genhtml -o doc/coverage doc/coverage/rcoverage.info
  chmod a+rx -R doc/
else
  echo "checkdb: $TALER_CHECKDB"
  TALER_AUDITORDB_POSTGRES_CONFIG=$TALER_CHECKDB \
  TALER_EXCHANGEDB_POSTGRES_CONFIG=$TALER_CHECKDB \
  TALER_BANK_ALTDB=$TALER_CHECKDB make check
fi
