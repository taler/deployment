#!/bin/bash

set -eu
components="deployment exchange merchant bank gnurl donations blog landing gnunet libmicrohttpd survey backoffice twister"

if [[ "$TALER_ENV_NAME" = test ]]; then
  components="$components playground help"
fi

for component in  $components ; do
  cd $HOME/$component
  git fetch
  if git status -sb | grep behind; then
    echo "invalidating $component"
    rm -f $HOME/deployment/taler-build/$component-stamp
  fi
done
