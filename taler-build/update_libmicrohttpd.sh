#!/bin/bash

cd $HOME/libmicrohttpd/
git clean -fdx

git fetch --all
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

./bootstrap
./configure --prefix=$HOME/local \
            --with-gnutls=/usr/local # Debian packages are too old.
make
make install
