#!/bin/bash

# Run as:
# $ ./update_twister.sh
set -eu

cd $HOME/twister/
git clean -fdx

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

git submodule update --init --force

./bootstrap
./configure CFLAGS='-ggdb -O0' \
  --prefix=$HOME/local \
  --with-gnunet=$HOME/local \
  --with-exchange=$HOME/local \
  --enable-logging=verbose
make install check
