#!/bin/bash

set -eu

cd $HOME/gnunet/
git clean -fdx

git fetch
# reset to updated upstream branch, but only if we're tracking a branch
branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
git reset --hard "$branch"

./bootstrap
./configure --prefix=$HOME/local \
            --enable-logging=verbose \
            --with-libgnurl=$HOME/local \
            --with-microhttpd=$HOME/local \
            --disable-documentation

make install -j1
