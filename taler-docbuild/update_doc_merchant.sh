#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
  git reset --hard "$branch"
}

cd $HOME/merchant
fetch
./bootstrap
./configure --enable-only-doc
make
make html pdf
doxygen
cp doc/*.pdf $HOME/build/merchant-backend/manual/pdf/
cp doc/*.{html,png,css,js} $HOME/build/merchant-backend/manual/html/
cp -r doxygen-doc/html/* $HOME/build/merchant-backend/doxygen/
