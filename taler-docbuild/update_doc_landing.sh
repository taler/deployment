#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo head)
  git reset --hard "$branch"
}

cd $HOME/docs-landing/
fetch
git submodule update --force --init

# Get the MOs files.
make compile

# Get the translated final HTMLs.
make translate

# No compilation needed so far.
mkdir -p $HOME/build/docs-landing/
cp -rt $HOME/build/docs-landing/ *.{css,svg} en/ fr/ it/ es/ pt/ ru/ de/ images/
