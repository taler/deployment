#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo head)
  git reset --hard "$branch"
}

cd $HOME/backoffice/doc/
fetch
make
mkdir -p $HOME/build/backoffice/pdf
mkdir -p $HOME/build/backoffice/html
cp manual.pdf $HOME/build/backoffice/pdf/
cp manual.html $HOME/build/backoffice/html/
cp *.css $HOME/build/backoffice/html/
