#!/bin/bash

set -eu
components="tutorials merchant exchange backoffice stage.taler.net www.taler.net api docs-landing"

for component in  $components ; do
  cd $HOME/$component
  git fetch
  if git status -sb | grep behind; then
    echo "invalidating $component"
    rm -f $HOME/deployment/taler-docbuild/$component-stamp
  fi
done
