#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
  git reset --hard "$branch"
}

cd $HOME/tutorials
fetch
make
cp php/doc/tutorial.pdf $HOME/build/merchant-frontend/php/pdf/
cp php/doc/tutorial.html $HOME/build/merchant-frontend/php/html/
cp php/doc/*.css $HOME/build/merchant-frontend/php/html/
cp php/doc/*.jpg $HOME/build/merchant-frontend/php/html/
cp python/doc/tutorial.pdf $HOME/build/merchant-frontend/python/pdf/
cp python/doc/tutorial.html $HOME/build/merchant-frontend/python/html/
cp python/doc/*.css $HOME/build/merchant-frontend/python/html/
cp python/doc/*.jpg $HOME/build/merchant-frontend/python/html/
