#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo HEAD)
  git reset --hard "$branch"
}

cd $HOME/exchange
fetch
./bootstrap
./configure --enable-only-doc
make pdf
make html
doxygen
cp doc/taler-exchange.pdf $HOME/build/exchange/manual/pdf/
cp doc/taler-exchange.html $HOME/build/exchange/manual/html/
cp doc/*.css $HOME/build/exchange/manual/html/
cp doc/*.png $HOME/build/exchange/manual/html/
cp doc/*.js $HOME/build/exchange/manual/html/
cp -r doxygen-doc/html/* $HOME/build/exchange/doxygen/
