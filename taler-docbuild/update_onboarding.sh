#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo head)
  git reset --hard "$branch"
}

cd $HOME/deployment
fetch
cd doc/
make all
cp *.css $HOME/build/onboarding/html/
cp onboarding.html $HOME/build/onboarding/html/
cp onboarding.pdf $HOME/build/onboarding/pdf/

