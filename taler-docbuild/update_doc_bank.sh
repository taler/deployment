#!/bin/bash

set -eu

fetch () {
  git clean -fdx
  git fetch
  # reset to updated upstream branch, but only if we're tracking a branch
  branch=$(git rev-parse --abbrev-ref --symbolic-full-name @{u} 2>/dev/null || echo head)
  git reset --hard "$branch"
}

cd $HOME/bank
fetch
./bootstrap
./configure --enable-only-doc
make pdf
make html
mkdir -p $HOME/build/bank/manual/pdf
mkdir -p $HOME/build/bank/manual/html
cp doc/taler-bank.pdf $HOME/build/bank/manual/pdf/
cp doc/taler-bank.html $HOME/build/bank/manual/html/
cp doc/*.css $HOME/build/exchange/manual/html/
