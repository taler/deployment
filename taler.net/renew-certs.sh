#!/usr/bin/env bash

set -eu

systemctl stop nginx

certbot certonly \
  --standalone \
  -d taler.net \
  -d www.taler.net \
  -d api.taler.net \
  -d lcov.taler.net \
  -d git.taler.net \
  -d gauger.taler.net \
  -d buildbot.taler.net \
  -d test.taler.net \
  -d playground.test.taler.net \
  -d auditor.test.taler.net \
  -d auditor.demo.taler.net \
  -d demo.taler.net \
  -d shop.test.taler.net \
  -d shop.demo.taler.net \
  -d survey.test.taler.net \
  -d survey.demo.taler.net \
  -d donations.demo.taler.net \
  -d backend.test.taler.net \
  -d backend.demo.taler.net \
  -d bank.test.taler.net \
  -d bank.demo.taler.net \
  -d www.git.taler.net \
  -d exchange.demo.taler.net \
  -d exchange.test.taler.net \
  -d env.taler.net \
  -d envs.taler.net \
  -d blog.demo.taler.net \
  -d blog.test.taler.net \
  -d donations.test.taler.net \
  -d docs.taler.net \
  -d intranet.taler.net \
  -d stage.taler.net

systemctl start nginx
