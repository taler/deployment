Deployment Layout
=================

top-level
---------

`green.*.taler.net`:  the green instance
`blue.*.taler.net`:  the blue instance
`shared-data`: static data shared between instances, such as private keys
`sockets`: directory with sockets of the active deployment, usually a symlink
to either the blue or green socket folder
