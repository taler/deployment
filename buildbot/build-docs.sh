#!/bin/bash

set -eu

cd $HOME/deployment

# like "git pull", but discard local changes
git fetch
git reset --hard FETCH_HEAD

cd $HOME/deployment/taler-docbuild
./invalidate.sh

make
