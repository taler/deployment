#!/bin/bash

active=$(sudo -u test cat /home/test/active)
nonactive="test-blue"
echo "Active party: ${active}"

if test $active = "test-blue"; then
  nonactive="test-green"
fi

PAUSE_LOCKFILE=/tmp/buildbot_pause.lock

if [ -f $PAUSE_LOCKFILE ]; then
echo "The Buildbot is paused, remove ${PAUSE_LOCKFILE} to resume."
# exit code 2 not really documented, just something
# different from the classical 1 (it's not a error after all).
exit 2
fi

sudo -u $nonactive bash -c 'source $HOME/activate; taler-deployment-build'
