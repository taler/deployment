#!/bin/bash

DEPLOYMENT="test"

if test "stable" = ${BRANCH}; then
  DEPLOYMENT="demo"

active=$(sudo -u ${DEPLOYMENT} cat /home/${DEPLOYMENT}/active)
nonactive="${DEPLOYMENT}-blue"
echo "Active party: ${active}"

if test $active = "${DEPLOYMENT}-blue"; then
  nonactive="${DEPLOYMENT}-green"
fi

cmd=$(printf "%s; %s" \
  "cd /home/test" \
  "./deployment/bin/taler-deployment-switch-${nonactive}")

echo "Switching to ${nonactive}."
sudo -u test bash -c "${cmd}"
