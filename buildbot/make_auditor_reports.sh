#!/bin/bash

# Logic that creates auditor reports, respecting
# the config file that lives under ${HOME}/.config/taler.conf.
#
# Therefore, if the config file mentions the "demo" ("test") DB,
# then the report will reflect the "demo" ("test") deployment.

# Exit on non-zero exits.
set -e

DATE=$(date +%Y-%m-%d)
REPORT_FILE_NOEXT=${HOME}/audit_report.${DATE}
WIRE_REPORT_FILE_NOEXT=${HOME}/wire_audit_report.${DATE}
JINJA_TEMPLATE="auditor-report.tex.j2"
REPORTS_DIRECTORY="${HOME}/reports"

source ${HOME}/activate

taler-auditor-exchange \
  -c ${HOME}/.config/taler.conf \
  -m $(taler-config -s exchange -o master_public_key) \
  -u $(taler-config -s exchange -o base_url)

# Generate first report.
taler-auditor > "${REPORT_FILE_NOEXT}.txt"

# Generate "wire" report.
taler-wire-auditor > "${WIRE_REPORT_FILE_NOEXT}.txt"

python ${PWD}/render_auditor_reports.py \
  "${REPORT_FILE_NOEXT}.txt" \
  "${WIRE_REPORT_FILE_NOEXT}.txt" \
  > "${REPORT_FILE_NOEXT}.tex" \
  < "${JINJA_TEMPLATE}"

cd ${HOME}
pdflatex "${REPORT_FILE_NOEXT}.tex"
# Give twice to let TeX resolve the sections references.
pdflatex "${REPORT_FILE_NOEXT}.tex"

# Remove garbage and set permissions. 
rm "${REPORT_FILE_NOEXT}.aux"

# Keep the log around.
# rm "${REPORT_FILE_NOEXT}.log"
chmod g+r "${REPORT_FILE_NOEXT}.pdf"
mv "${REPORT_FILE_NOEXT}.pdf" "${REPORTS_DIRECTORY}"

echo "The file ${REPORT_FILE_NOEXT}.pdf" has been correctly generated.
