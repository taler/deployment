#!/bin/bash


if test -z ${TALER_DEPLOYMENT}; then
  echo "Please export env variable TALER_DEPLOYMENT=(test|demo) before running this script"
  exit 1
fi

nonactive="dummy"
if test ${TALER_DEPLOYMENT} = "test"; then
  active=$(sudo -u ${TALER_DEPLOYMENT} cat /home/${TALER_DEPLOYMENT}/active)
  nonactive="blue"

  echo "Active party: ${active}"

  if test $active = "test-blue"; then
    nonactive="green"
  fi
fi

error_stringify ()
{
  case $1 in
    28) echo "connection timed out" ;;
     7) echo "failed to connect to host" ;;
     0) echo "not a error, curl went fine" ;;
     *) echo "unknown, see curl man page" ;;
  esac
}

error_fmt="%s (http status code: %s)/(curl exit code: %s - %s)\n"

URL="https://exchange.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s "$URL" -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "'%s' failed\n" $URL
  printf "$error_fmt" \
    "Exchange did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

URL="http://backend.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL \
  --header "Authorization: ApiKey sandbox" \
  -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "'%s' failed\n" $URL
  printf "$error_fmt" \
    "Merchant backend did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi


URL="https://shop.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "%s failed" $URL
  printf "$error_fmt" \
    "Blog did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

URL="https://survey.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "%s failed" $URL
  printf "$error_fmt" \
    "Survey site did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

URL="https://donations.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "%s failed" $URL
  printf "$error_fmt" \
    "Donations shop did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

URL="https://bank.${TALER_DEPLOYMENT}.taler.net/"
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL -o /dev/null \
  -w "%{http_code}")
if ! test 302 = $http_status_code; then
  printf "%s failed" $URL
  printf "$error_fmt" \
    "Bank did not restart correctly" \
    $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

URL="https://${TALER_DEPLOYMENT}.taler.net/en/index.html" 
http_status_code=$(curl \
  -H "X-Taler-Deployment-Color: $nonactive" \
  -s $URL -o /dev/null \
  -w "%{http_code}")
if ! test 200 = $http_status_code; then
  printf "%s failed" $URL
  printf "$error_fmt" \
    "Landing page not restart correctly" \
     $http_status_code $? "$(error_stringify $?)"
  exit 1
fi

printf "All services correctly restarted!\n"
