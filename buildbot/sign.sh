#!/bin/bash

DEPLOYMENT="test"

if test "stable" = ${BRANCH}; then
  DEPLOYMENT="demo"

active=$(sudo -u ${DEPLOYMENT} cat /home/${DEPLOYMENT}/active)
nonactive="${DEPLOYMENT}-blue"
echo "Active party: ${active}"

if test $active = "${DEPLOYMENT}-blue"; then
  nonactive="${DEPLOYMENT}-green"
fi

sudo -u $nonactive bash -c 'source $HOME/activate; taler-deployment-config-sign'
