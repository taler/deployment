(define-module (custom-packages postfix)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages cyrus-sasl)
  #:use-module (gnu packages openldap)
  #:use-module (guix)
  #:use-module (guix utils)
  #:use-module (guix build-system gnu)
  #:use-module ((guix licenses) #:prefix license:))


(define-public postfix
  (package
   (name "postfix")
   (version "3.3.2")
   (source (origin
            (method url-fetch)
            (uri (string-append
                  "http://cdn.postfix.johnriley.me/mirrors/postfix-release/official/postfix-"
                  version ".tar.gz"))
            (sha256 (base32
                     "0nxkszdgs6fs86j6w1lf3vhxvjh1hw2jmrii5icqx9a9xqgg74rw"))))
   (native-inputs
     `(("m4" ,m4)))
   (inputs
     `(("bdb" ,bdb)
       ("openssl" ,openssl)
       ("sqlite" ,sqlite)
       ("pcre" ,pcre)
       ("postgresql" ,postgresql)
       ("openldap" ,openldap)
       ("cyrus-sasl" ,cyrus-sasl)
       ("lmdb" ,lmdb)))
   (build-system gnu-build-system)
   (arguments
     `(#:tests? #f ; Postfix does not come with any tests.
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           ;; Postfix does not have a standard "./configure".
           (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
             (define (dir-setting name dir)
	       (string-append name "=" (assoc-ref outputs "out") dir))
	     (invoke
	       "make"
	       "makefiles"
	       (string-append "SHELL=" (which "sh"))
	       (dir-setting "daemon_directory" "/libexec/postfix")
	       (dir-setting "shlib_directory" "/lib/postfix")
	       (dir-setting "command_directory" "/sbin")
	       (dir-setting "manpage_directory" "/share/man")
	       (dir-setting "newaliases_path" "/bin/newaliases")
	       (dir-setting "mailq_path" "/bin/mailq")
	       (dir-setting "sendmail_path" "/sbin/sendmail")
	       (string-append
		 "CCARGS="
		 (string-join
		   (list
		     "-DHAS_DB"
		     "-DHAS_LMDB"
		     "-DHAS_PGSQL"
		     "-DHAS_PCRE"
		     "-DHAS_LDAP"
		     "-DHAS_SQLITE"
		     "-DUSE_TLS"
		     "-DUSE_SASL_AUTH"
		     "-DUSE_CYRUS_SASL"
		     ;; only the default, can be changed at run time
		     "-DDEF_SERVER_SASL_TYPE=\\\"dovecot\\\""
		     "-DNO_NIS"
		     (string-append
		       "-I"
		       (assoc-ref inputs "cyrus-sasl")
		       "/include/sasl"))
		   " "))
	       "shared=yes"
	       (string-append
		 "SHLIB_RPATH=-Wl,-rpath,"
		 (assoc-ref outputs "out")
		 "/lib/postfix")
	       "dynamicmaps=yes"
	       "AUXLIBS=-ldb -lresolv -lssl -lcrypto -lsasl2"
	       "AUXLIBS_LMDB=-llmdb"
	       "AUXLIBS_LDAP=-lldap -llber"
	       "AUXLIBS_PCRE=-lpcre"
	       "AUXLIBS_PGSQL=-lpq"
	       "AUXLIBS_SQLITE=-lsqlite3 -lpthread")))
         (replace 'install
           ;; Postfix's "make install" is interactive, we work around this
           ;; by directly calling postfix-install with the right arguments.
           (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
	     (substitute* "postfix-install"
	      (("^SHELL=/bin/sh$") "SHELL=sh")
	      (("^PATH=.*$") ""))
	     (setenv "LD_LIBRARY_PATH"
		     (string-append (getcwd) "/lib"))
	     (invoke
	       "sh"
	       "postfix-install"
	       (string-append "install_root=" (assoc-ref outputs "out"))
	       "daemon_directory=/libexec/postfix"
	       "command_directory=/sbin"
	       "manpage_directory=/share/man"
	       "newaliases_path=/bin/newaliases"
	       "mailq_path=/bin/mailq"
	       "sendmail_path=/sbin/sendmail"
	       "shlib_directory=/lib/postfix"
	       "-non-interactive"
	       "-package")))
         (add-after 'install 'patch-master-cf
           ;; Make sure that the default main.cf does not contain wrong/confusing
           ;; paths.
           (lambda* (#:key outputs inputs configure-flags #:allow-other-keys)
             (define comment
               "# Note for Guix: This parameter should usually not be
# changed, as the compiled-in default in the postfix
# binaries already points to the Guix store.")
	     (substitute* (string-append
                            (assoc-ref outputs "out")
                            "/etc/postfix/main.cf")
              (("^daemon_directory ?=" m) (string-append comment "\n#" m))))))))
   (synopsis "High-performance mail transport agent")
   (description
    "Postfix is Wietse Venema's mail transport agent that started
     life as an alternative to the widely-used Sendmail program.
     Postfix attempts to be fast, easy to administer, and secure,
     while at the same time being sendmail compatible enough to
     not upset existing users. Thus, the outside has a sendmail-ish
     flavor, but the inside is completely different.")
   (license license:ibmpl1.0)
   (home-page "http://www.postfix.org/")))
