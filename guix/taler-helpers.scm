(define-module (taler-helpers)
  #:use-module (guix)
  #:use-module (guix utils)
  #:use-module (ice-9 textual-ports)
  #:export (concat-local-files))

;;;
;;; Helpers
;;;

(define (absolute-file-name file directory)
  "Return the canonical absolute file name for FILE, which lives in the
vicinity of DIRECTORY."
  (canonicalize-path
   (cond ((string-prefix? "/" file) file)
         ((not directory) file)
         ((string-prefix? "/" directory)
          (string-append directory "/" file))
         (else file))))

(define (%%concat-local-files srcdir outname files)
  (define (slurp f)
    (call-with-input-file (absolute-file-name f srcdir) get-string-all))
  (define (file-concat files)
    (string-concatenate (map slurp files)))
  (plain-file outname (file-concat files)))


(define-syntax concat-local-files
  (lambda (s)
    (syntax-case s ()
      ((_ outname files)
       #'(%%concat-local-files (current-source-directory) outname files))
      ((_)
       #'(syntax-error "missing arguments"))
      (id
       (identifier? #'id)
       #'(syntax-error
          "'concat-local-files' is a macro and cannot be used like this")))))
