;;; GNU Guix system administration tools.
;;;
;;; Copyright © 2016, 2017 Ludovic Courtès <ludo@gnu.org>
;;;
;;; This program is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-module (sysadmin people)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (gnu services ssh)
  #:use-module (gnu packages base)
  #:use-module (ice-9 match)
  #:export (sysadmin?
            sysadmin
            sysadmin-service-type))

;;; Commentary:
;;;
;;; Declaration of system administrator user accounts.
;;;
;;; Code:

(define-record-type* <sysadmin> sysadmin make-sysadmin
  sysadmin?
  (name            sysadmin-name)
  (full-name       sysadmin-full-name)
  (ssh-public-key  sysadmin-ssh-public-key)
  (restricted?     sysadmin-restricted? (default #f)))

(define (sysadmin->account sysadmin)
  "Return the user account for SYSADMIN."
  (match sysadmin
    (($ <sysadmin> name comment _ restricted?)
     (user-account
      (name name)
      (comment comment)
      (group "users")
      (supplementary-groups (if restricted?
                                '()
                                '("wheel" "kvm"))) ;sudoer
      (home-directory (string-append "/home/" name))))))

(define (sysadmin->authorized-key sysadmin)
  "Return an authorized key tuple for SYSADMIN."
  (list (sysadmin-name sysadmin)
        (sysadmin-ssh-public-key sysadmin)))

(define sysadmin-service-type
  ;; The service that initializes sysadmin accounts.
  (service-type
   (name 'sysadmin)
   (extensions (list (service-extension account-service-type
                                        (lambda (lst)
                                          (map sysadmin->account lst)))
                     (service-extension openssh-service-type
                                        (lambda (lst)
                                          (map sysadmin->authorized-key
                                               lst)))))))

;;; people.scm ends here
